/** @file plugin_rhythmbox.h
 *  @brief defines a plugin to control Rhythmbox
 */

#ifndef __BIGHCI_PLUGIN_TOTEM_H__
#define __BIGHCI_PLUGIN_TOTEM_H__

#include <X11/Xlib.h>
#include <rcix11.h>
#include <plugin_mpris_template.h>

namespace Rci
{

/** @brief This class defines a Plugin which communicates with rhythmbox using Dbus
 */
class Plugin_VLC : protected RciX11, public MprisTemplate
{
public:
  static Rci::Plugin_Base *maker();   ///< Factory
  static void eraser(Rci::Plugin_Base *in_Ptr);   ///< Factory

  Plugin_VLC();
  ~Plugin_VLC();
  
protected:
  virtual void xkeysym( bool, CmdArguments const&, std::string *, CmdAnswer * );
};

}//namespace Rci

#endif //__BIGHCI_PLUGIN_RHYTHMBOX_H__
