#include <dbus/dbus.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "plugin_vlc.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::Plugin_Base *maker()   ///< Factory
{
  return Rci::Plugin_VLC::maker();
}

void eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  Rci::Plugin_VLC::eraser(in_Ptr);
}
#ifdef __cplusplus
}
#endif

Rci::Plugin_Base *Rci::Plugin_VLC::maker()   ///< Factory
{
  Rci::Plugin_Base *ans = new Rci::Plugin_VLC;
  return ans;
}

void Rci::Plugin_VLC::eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  delete in_Ptr;
}

Rci::Plugin_VLC::Plugin_VLC() : RciX11(),
  MprisTemplate("VLC", "org.mpris.MediaPlayer2.vlc","/org/mpris/MediaPlayer2")
{
  addState( "xkeysym", boost::bind(&Plugin_VLC::xkeysym, this, _1, _2, _3, _4) );
}

Rci::Plugin_VLC::~Plugin_VLC(){;}

void Rci::Plugin_VLC::xkeysym
  (bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers)
{
  _mediaplayer->Raise();  
  
  if( inArguments.size() >= 1 + _skipArgument )
    sendTestFakeKeyEvent(inArguments[0 + _skipArgument]);
  else
    _logger.logIt("Argument(s) missing", LgCpp::Logger::logWARNING);
}
